**Prime Number Util**

A simple HTML which incorporates Material Design for Bootstrap and Javascript to generate and verify prime numbers.

---

## Prime Generation

Generate prime numbers within a range defined by the user. The start range will be defaulted to 1 if the value provided is non positive or exceeds the end range value. 
The generated prime number DOM rendering is capped at **5000** elements to ensure the smoothness of the page.

---

## Prime Determination

Determines if a number given is prime or composite.
